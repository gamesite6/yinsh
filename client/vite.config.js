import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/ringmaster/",

  build: {
    emptyOutDir: false,
    lib: {
      entry: "src/index.ts",
      fileName: "ringmaster",
      name: "Gamesite6_Ringmaster",
    },
  },
  css: {
    modules: {},
  },
  plugins: [svelte()],
});
